﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace FileXmlApp
{
   public class Program
    {
       public static void Main(string[] args)
        {
            Stopwatch sw = Stopwatch.StartNew();
            SqlConnection connection;
            SqlDataReader reader;

            try
            {
                int id;

                Console.WriteLine("Ano");
                var ano = Console.ReadLine();

                for (int anoProcessado = Convert.ToInt32(ano); anoProcessado < Convert.ToInt32(DateTime.Now.Year); anoProcessado++)
                {

                    var mes = 12;

                    while (mes>=1)
                    {
                        Console.WriteLine("Processando xml para: Ano: "+ anoProcessado + " & Mes:" + mes.ToString());

                        var zeroAuxiliar = "";

                        if (mes<10)
                        {
                            zeroAuxiliar = "0";
                        }
                        var dataInicial = anoProcessado + zeroAuxiliar +mes.ToString() + "01";
                        var dataFinal = anoProcessado + zeroAuxiliar + mes.ToString() + Convert.ToString(new DateTime(Convert.ToInt32(anoProcessado), mes, DateTime.DaysInMonth(Convert.ToInt32(anoProcessado), mes)).Day);

                        connection = new SqlConnection(Properties.Settings.Default.MyConnection);
                        connection.Open();

                        reader = new SqlCommand(string.Format(@"SELECT * 
                                            FROM   MSAF_XML_NFCE_SAT [mxns]
                                            WHERE [mxns].[DATA_VENDA] BETWEEN '{0}' AND '{1}' ", dataInicial, dataFinal), connection).ExecuteReader();
                        var contador = 0;

                        if (reader.HasRows)
                        {
                            var path = Properties.Settings.Default.PathToSave;

                            path = path + anoProcessado + @"\" + mes.ToString() + @"\";

                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }


                            DirectoryInfo diretorio = new DirectoryInfo(path);

                            DataTable dt = new DataTable();
                            dt.Clear();
                            dt.Columns.Add("Name");


                            foreach (FileInfo flInfo in diretorio.GetFiles(".xml"))
                            {
                                var name = flInfo.Name;
                                string[] separaExtensao = name.Split('.');
                                var nomeArquivo = separaExtensao[0].ToString();

                                DataRow colNameArquivo = dt.NewRow();
                                colNameArquivo["Name"] = nomeArquivo;
                                dt.Rows.Add(colNameArquivo);
                            }

                            while (reader.Read())
                            {
                                XmlDocument xdoc = new XmlDocument();
                                var nomeItemTable = reader.GetString(1);

                                var HasInTable = from myRow in dt.AsEnumerable()
                                                 where myRow.Field<string>("Name") == nomeItemTable
                                                 select myRow.Field<string>("Name").Count();

                                var nomeArquivoPasta = "";
                                if (!HasInTable.Any())
                                {
                                    xdoc.LoadXml(reader.GetString(1));

                                    XmlNodeList nodeList = (xdoc.SelectNodes("//infCFe").Count < 1 ? xdoc.SelectNodes("//infNFe") : xdoc.SelectNodes("//infCFe"));

                                    if (nodeList.Count == 1)
                                    {
                                    foreach (XmlNode node in nodeList)
                                    {
                                        nomeArquivoPasta = node.Attributes["Id"].Value.Remove(0, 3);
                                    }
                                    contador++;
                                    xdoc.Save(path + nomeArquivoPasta + "_" + reader.GetInt32(3).ToString() + ".xml");
                                    }
                                    //else
                                    //{
                                    //    xdoc.Save(path + reader.GetString(0) + "_" + reader.GetInt32(3).ToString() + ".xml");
                                    //}
                                }

                            }
                        }
                        else
                        {
                            Console.WriteLine("No rows found.");
                        }
                        reader.Close();
                        mes--;
                        Console.WriteLine(Convert.ToString(contador)+ ": Xml processados.");
                    }
            }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            sw.Stop();
            Console.WriteLine(sw.Elapsed);
            Console.WriteLine("Fim de processamento, clique qualquer tecla para sair.");
            Console.ReadKey();

        }
    }
}
